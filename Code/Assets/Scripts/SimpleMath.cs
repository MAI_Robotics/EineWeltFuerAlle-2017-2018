﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SimpleMath : MonoBehaviour
{
    // Menu and Game
    public GameObject play;
    public GameObject home;

    // UI Game
    public Slider timeDisplay;
    public Text calculation;
    public Text result;
    public Text scoreText;

    // UI Home
    public Text highScoreText;

    // Options
    public float time;

    // Vars
    private int part1;
    private int part2;
    private float produkt;
    private float comparisonswert;
    private float timer;
    private int rightButton;
    private int score;
    private int highScore;
    private bool playModus;


    private void Start()
    {
        // set slider size
        timeDisplay.maxValue = time;
        timeDisplay.value = 0;
        timer = time;
        highScore = PlayerPrefs.GetInt("SimpleMath High Score");
        highScoreText.text = highScore.ToString();
        playModus = false;
    }

    private void Update ()
    {
        if (playModus)
        {
            Timer();
        }
	}

    private void CalculationPlus() {
        part1 = Random.Range(-50, 100);
        part2 = Random.Range(-50, 100);
        produkt = part1 + part2;
        scoreText.text = score.ToString();

        calculation.text = part1.ToString () + " + " + part2.ToString ();

        comparisonswert = produkt + Random.Range(-2, 2);

        result.text = "= " + comparisonswert.ToString ();

        if (comparisonswert == produkt) {
            rightButton = 0;
        } else {
            rightButton = 1;
        }
    }

    private void CalculationMinus()
    {
        part1 = Random.Range(-50, 100);
        part2 = Random.Range(-25, 25);
        produkt = part1 - part2;
        scoreText.text = score.ToString();

        calculation.text = part1.ToString() + " - " + part2.ToString();

        comparisonswert = produkt + Random.Range(-2, 2);

        result.text = "= " + comparisonswert.ToString();

        if (comparisonswert == produkt)
        {
            rightButton = 0;
        }
        else
        {
            rightButton = 1;
        }
    }

    private void CalculationMultiply()
    {
        part1 = Random.Range(-2, 10);
        part2 = Random.Range(0, 10);
        produkt = part1 * part2;
        scoreText.text = score.ToString();

        calculation.text = part1.ToString() + " * " + part2.ToString();

        comparisonswert = produkt + Random.Range(-2, 2);

        result.text = "= " + comparisonswert.ToString();

        if (comparisonswert == produkt)
        {
            rightButton = 0;
        }
        else
        {
            rightButton = 1;
        }
    }

    private void CalculationDivide()
    {
        part1 = Random.Range(1, 10);
        part2 = Random.Range(1, 10);
        produkt = part1 / part2;
        scoreText.text = score.ToString();

        calculation.text = part1.ToString() + " / " + part2.ToString();

        comparisonswert = produkt + (float) Random.Range(-2, 2);

        result.text = "= " + comparisonswert.ToString();

        if (comparisonswert == produkt)
        {
            rightButton = 0;
        }
        else
        {
            rightButton = 1;
        }
    }

    private void generateCalc()
    {
        switch (Random.Range(1, 4))
        {
            case 1:
                CalculationDivide();
                break;
            case 2:
                CalculationMinus();
                break;
            case 3:
                CalculationMultiply();
                break;
            case 4:
                CalculationPlus();
                break;
        }
    }

    public void Button (int comparison)
    {
        if (rightButton == comparison)
        {
            generateCalc();
            score = score + 1;
            scoreText.text = score.ToString ();
            timer = time;
        }
        else
        {
            GameOver();
        }
    }

    private void Timer ()
    {

        timer -= Time.deltaTime;
        timeDisplay.value = timer;
        
        if (timer <= 0)
        {
            playModus = false;
            GameOver();
        }
    }

    private void GameOver ()
    {
        play.SetActive(false);
        home.SetActive(true);
        if (score > highScore)
        {
            highScore = score;
            highScoreText.text = highScore.ToString ();
        }
    }

    public void lodeMenu ()
    {
        PlayerPrefs.SetInt("SimpleMath High Score", highScore);
        Application.LoadLevel("Menu");
    }

    public void PlayAgian ()
    {
        score = 0;
        generateCalc();
        timer = time;
        scoreText.text = score.ToString();
        play.SetActive(true);
        home.SetActive(false);
        playModus = true;

    }
}
