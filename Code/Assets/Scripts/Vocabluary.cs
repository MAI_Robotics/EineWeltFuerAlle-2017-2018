﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Vocabluary : MonoBehaviour
{
    public Image picture;
    public Button[] buttons;
    public Sprite[] vocabPictures;

    Button btn1;
    Button btn2;
    Button btn3;
    Sprite[] images;
    Sprite img;
    Image ImagePlaceholder;

    private string[] vocabs;
    private int rightButton;
	
	void Start ()
    {
        vocabs = new string[vocabPictures.Length];

        btn1 = buttons[0];
        btn2 = buttons[1];
        btn3 = buttons[2];

        images = vocabPictures;
        ImagePlaceholder = picture;

        for (int i = 0; i < vocabPictures.Length; i++)
        {
            vocabs[i] = vocabPictures[i].name;
        }
        GenerateVocsNoDouble();
    }

    public void TestAnswer(int i)
    {
        if (i == rightButton)
        {
            buttons[rightButton].GetComponent<Image>().color = Color.green;
            StartCoroutine(Wait());
        }
        else
        {
            buttons[i].GetComponent<Image>().color = Color.red;
            buttons[rightButton].GetComponent<Image>().color = Color.green;
            StartCoroutine(Wait());
        }
    }

    public void GenerateVocsNoDouble()
    {
        img = images[Random.Range(1, images.Length + 1) - 1];
        ImagePlaceholder.sprite = img;

        int ran = Random.Range(1, 9);
        if (ran == 1 || ran == 4 || ran == 7)
        {
            string btn1text = img.name;
            btn1.GetComponentInChildren<Text>().text = btn1text;

            string btn2text = images[Random.Range(1, images.Length + 1) - 1].name;
            while (btn2text == btn1text)
            {
                btn2text = images[Random.Range(1, images.Length + 1) - 1].name;
            }
            btn2.GetComponentInChildren<Text>().text = btn2text;

            string btn3text = images[Random.Range(1, images.Length + 1) - 1].name;
            while (btn3text == btn1text || btn3text == btn2text)
            {
                btn3text = images[Random.Range(1, images.Length + 1) - 1].name;
            }
            btn3.GetComponentInChildren<Text>().text = btn3text;

            rightButton = 0;
        }
        else if (ran == 2 || ran == 5 || ran == 8)
        {
            string btn2text = img.name;
            btn2.GetComponentInChildren<Text>().text = btn2text;

            string btn1text = images[Random.Range(1, images.Length + 1) - 1].name;
            while (btn1text == btn2text)
            {
                btn1text = images[Random.Range(1, images.Length + 1) - 1].name;
            }
            btn1.GetComponentInChildren<Text>().text = btn1text;

            string btn3text = images[Random.Range(1, images.Length + 1) - 1].name;
            while (btn3text == btn1text || btn3text == btn2text)
            {
                btn3text = images[Random.Range(1, images.Length + 1) - 1].name;
            }
            btn3.GetComponentInChildren<Text>().text = btn3text;

            rightButton = 1;
        }
        else
        {
            string btn3text = img.name;
            btn3.GetComponentInChildren<Text>().text = btn3text;

            string btn1text = images[Random.Range(1, images.Length + 1) - 1].name;
            while (btn1text == btn3text)
            {
                btn1text = images[Random.Range(1, images.Length + 1) - 1].name;
            }
            btn1.GetComponentInChildren<Text>().text = btn1text;

            string btn2text = images[Random.Range(1, images.Length + 1) - 1].name;
            while (btn2text == btn1text || btn2text == btn3text)
            {
                btn2text = images[Random.Range(1, images.Length + 1) - 1].name;
            }
            btn2.GetComponentInChildren<Text>().text = btn2text;
            rightButton = 2;
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2f);
        GenerateVocsNoDouble();
        for(int i = 0;i< buttons.Length; i++)
        {
            buttons[i].GetComponent<Image>().color = Color.grey;
        }
    }
}
