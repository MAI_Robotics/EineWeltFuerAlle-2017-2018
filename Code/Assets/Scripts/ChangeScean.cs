﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScean : MonoBehaviour
{
    private int clickToClose = 10;

    public void lodeSimpleMath ()
    {
        Application.LoadLevel("SimpleMath");
    }

    public void lodeMemory ()
    {
        Application.LoadLevel("Memory");
    }

    public void loadVocabs ()
    {
        Application.LoadLevel("Vocabulary");
    }

    public void LoadMenu ()
    {
        Application.LoadLevel("Menu");
    }

    public void Quit()
    {
        #if (UNITY_EDITOR || DEVELOPMENT_BUILD)
                Debug.Log(this.name + " : " + this.GetType() + " : " + System.Reflection.MethodBase.GetCurrentMethod().Name);
        #endif
        #if (UNITY_EDITOR)
                UnityEditor.EditorApplication.isPlaying = false;
        #elif (UNITY_STANDALONE)
            Application.Quit();
        #elif (UNITY_WEBGL)
            if(clickToClose <= 0) {
                Application.OpenURL("about:blank");
            } else {
                clickToClose--;
            }
        #endif
    }

}
