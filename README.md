# MAI-Robotics Learning App

1. Installation
2. Build
3. Copyright and Disclaimer
4. Images

---

## 1. Installation

1. Download this project or clone it via git
2. Open Unity
3. Open the folders
4. Install necessary libraries and additional packages

## 2. Build

We build the app for demo purposes for the RaspberryPi 3B in WebGL.

To run it on the Pi you have to enable the GL Driver in the *raspi-config*.
-> *sudo raspi-config*
-> 7 Advanced Options
-> A7 GL Driver
-> G2 GL (Fake KMS)
-> reboot

We recommend running it in Firefox since it is the easiest. Just open the index.html file

## 3. Cpoyright and Disclaimer

- We built this app for demo purpose **only**!
- This software was developed as part of a school project for educational purposes. 
- You can reuse it in your hardware or software projects.
- It is forbidden to sell our software! 
- The code might be based on other open source projects or tutorials. It is no copy!
- The images are taken from [pexels.com](https://www.pexels.com) and licensed under [this license.](https://www.pexels.com/photo-license/)
- We are **not** responsible for any reuse of this app or any damage it may produce.

## 4. Images
https://www.pexels.com/photo/person-holding-armak-r100-basketball-ball-1462618/
https://www.pexels.com/de/foto/ara-bunt-draussen-exotisch-1462052/
https://www.pexels.com/photo/person-holding-gray-polaroid-land-camera-1458716/
https://www.pexels.com/de/foto/asphalt-ausweichen-auto-automobil-1409990/#
https://www.pexels.com/photo/short-coated-grey-cat-lying-on-green-grass-field-1503502/
https://www.pexels.com/de/foto/abenteuer-aktivitat-beratung-campen-1308751/
https://www.pexels.com/de/foto/augen-bezaubernd-gucken-hauslich-1471984/
https://www.pexels.com/photo/photography-of-pink-doughnut-928475/
https://www.pexels.com/photo/aerial-photography-of-forest-1468704/
https://www.pexels.com/photo/person-holding-grey-hair-dryer-973406/
https://www.pexels.com/photo/man-riding-horse-1462364/
https://www.pexels.com/photo/person-using-iphone-1194760/
https://www.pexels.com/de/foto/justifyyourlove-lampe-mauer-nahansicht-823841/
https://www.pexels.com/de/foto/arbeitsplatz-bildschirm-buro-business-1466609/
https://www.pexels.com/photo/lion-sleeping-beside-rock-1187987/
https://www.pexels.com/photo/mountain-range-and-river-1461035/
https://www.pexels.com/photo/phtoto-of-person-holding-orange-pumpkin-ornament-1485697/
https://www.pexels.com/photo/selective-focus-photo-of-person-wearing-pink-nike-low-top-sneakers-1478441/
https://www.pexels.com/photo/yellow-sunflower-1462246/
https://www.pexels.com/de/foto/bahnhof-business-eisenbahn-eisenbahnschienen-1463008/
https://www.pexels.com/photo/photo-of-swiss-cheese-leaf-1484671/
